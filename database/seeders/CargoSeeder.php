<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Cargo;
class CargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
          Cargo::create([
            'codigo' => 'C001',
            'nombre' => 'Administrador',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Cargo::create([
            'codigo' => 'C002',
            'nombre' => 'Lider Frontend',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Cargo::create([
            'codigo' => 'C003',
            'nombre' => 'Desarrollador Frontend',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Cargo::create([
            'codigo' => 'C004',
            'nombre' => 'Abogado',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Cargo::create([
            'codigo' => 'C005',
            'nombre' => 'Guardia',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Cargo::create([
            'codigo' => 'C006',
            'nombre' => 'Pollero',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

    }
}
