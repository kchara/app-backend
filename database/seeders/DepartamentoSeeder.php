<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Departamento;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Departamento::create([
            'codigo' => 'D001',
            'nombre' => 'Tecnologias de la Informacion',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Departamento::create([
            'codigo' => 'D002',
            'nombre' => 'Legal',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Departamento::create([
            'codigo' => 'D003',
            'nombre' => 'Seguridad',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

        Departamento::create([
            'codigo' => 'D004',
            'nombre' => 'Eventos y Buffets',
            'activo' => true,
            'idUsuarioCreacion' => 1,
        ]);

    }
}
