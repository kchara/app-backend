<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Departamento;
use App\Models\Cargo;

class Usuario extends Model
{
    use HasFactory;

    protected $fillable = [
        'usuario', 'primerNombre', 'segundoNombre',
        'primerApellido', 'segundoApellido', 'idDepartamento', 'idCargo', 'email'
    ];

    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'idDepartamento');
    }

    public function cargo()
    {
        return $this->belongsTo(Cargo::class, 'idCargo');
    }
}
