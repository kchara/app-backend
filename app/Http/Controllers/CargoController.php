<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cargo;
use App\Http\Requests\CargoRequest;

class CargoController extends Controller
{
    public function index()
    {
        $cargos = Cargo::all();
        return response()->json($cargos);
    }

    public function store(CargoRequest $request)
    {
        $cargo = Cargo::create($request->validated());
        return response()->json($cargo, 201);
    }

    public function show(Cargo $cargo)
    {
        return response()->json($cargo);
    }

    public function update(CargoRequest $request, Cargo $cargo)
    {
        $cargo->update($request->validated());
        return response()->json($cargo);
    }

    public function destroy(Cargo $cargo)
    {
        $cargo->delete();
        return response()->json(null, 204);
    }
}
