<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Departamento;
use App\Http\Requests\DepartamentoRequest; 

class DepartamentoController extends Controller
{ 
    public function index()
    {
        $departamentos = Departamento::all();
        return response()->json($departamentos);
    }

    public function store(DepartamentoRequest $request)
    {
        $departamento = Departamento::create($request->validated());
        return response()->json($departamento, 201);
    }

    public function show($departamento)
    {
        return response()->json($departamento);
    }

    public function update(DepartamentoRequest $request, Departamento $departamento)
    {
        $departamento->update($request->validated());
        return response()->json($departamento);
    }

    public function destroy(Departamento $departamento)
    {
        $departamento->delete();
        return response()->json(null, 204);
    }
}
