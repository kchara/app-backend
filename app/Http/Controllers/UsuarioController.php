<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Http\Requests\UsuarioRequest;
use App\Http\Requests\UsuarioFilterRequest;

class UsuarioController extends Controller
{
    public function index(UsuarioFilterRequest $request)
    {
        // Obtener los parámetros de la solicitud
        $idDepartamento = $request->input('idDepartamento');
        $idCargo = $request->input('idCargo');

        $users = Usuario::join('departamentos', 'usuarios.idDepartamento', '=', 'departamentos.id')
                ->join('cargos', 'usuarios.idCargo', '=', 'cargos.id')
                ->when($idDepartamento, function ($query) use ($idDepartamento) {
                    return $query->where('usuarios.idDepartamento', '=', $idDepartamento);
                })
                ->when($idCargo, function ($query) use ($idCargo) {
                    return $query->where('usuarios.idCargo', '=', $idCargo);
                })
                ->select(
                    'usuarios.id',
                    'usuarios.usuario',
                    'usuarios.primerNombre',
                    'usuarios.segundoNombre',
                    'usuarios.primerApellido',
                    'usuarios.segundoApellido',
                    'usuarios.idDepartamento',
                    'usuarios.idCargo',
                    'usuarios.email',
                    'departamentos.nombre as nombreDepartamento',
                    'cargos.nombre as nombreCargo',

                )
                ->get();

        return response()->json($users);
    }

    public function store(UsuarioRequest $request)
    {
        $user = Usuario::create($request->validated());
        return response()->json($user, 201);
    }

    public function show(string $id)
    {
        return Usuario::findOrFail($id);   
    }

    public function update(UsuarioRequest $request, string $id)
    {
        $user = Usuario::findOrFail($id);
        $user->update($request->all());
        return response()->json($user, 200);
    }

    public function destroy(string $id)
    {
        $user = Usuario::findOrFail($id);
        $user->delete();
        return response()->json($user, 200); 
    }
}
