<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
 
    public function rules(): array
    {
        return [
            'usuario' => 'required|string',
            'primerNombre' => 'required|string',
            'segundoNombre' => 'string|nullable',
            'primerApellido' => 'required|string',
            'segundoApellido' => 'string|nullable',
            'idDepartamento' => 'required|integer',
            'idCargo' => 'required|integer',
            'email' => 'required|email|unique:usuarios,email',
        ];
    }
}
