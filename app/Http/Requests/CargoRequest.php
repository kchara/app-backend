<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CargoRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'codigo' => 'required|string',
            'nombre' => 'required|string',
            'activo' => 'boolean',
            'idUsuarioCreacion' => 'required|integer',
        ];
    }
}
