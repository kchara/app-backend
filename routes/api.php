<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DepartamentoController;
use App\Http\Controllers\CargoController;
use App\Http\Controllers\UsuarioController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Rutas para Departamentos
Route::get('/departamentos', [DepartamentoController::class, 'index']);
Route::post('/departamentos', [DepartamentoController::class, 'store']);
Route::get('/departamentos/{departamento}', [DepartamentoController::class, 'show']);
Route::put('/departamentos/{departamento}', [DepartamentoController::class, 'update']);
Route::delete('/departamentos/{departamento}', [DepartamentoController::class, 'destroy']);

// Rutas para Cargos
Route::get('/cargos', [CargoController::class, 'index']);
Route::post('/cargos', [CargoController::class, 'store']);
Route::get('/cargos/{cargo}', [CargoController::class, 'show']);
Route::put('/cargos/{cargo}', [CargoController::class, 'update']);
Route::delete('/cargos/{cargo}', [CargoController::class, 'destroy']);

// Rutas para Usuarios
Route::get('/usuarios', [UsuarioController::class, 'index']);
Route::post('/usuarios', [UsuarioController::class, 'store']);
Route::get('/usuarios/{usuario}', [UsuarioController::class, 'show']);
Route::put('/usuarios/{usuario}', [UsuarioController::class, 'update']);
Route::delete('/usuarios/{usuario}', [UsuarioController::class, 'destroy']);


